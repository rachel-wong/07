## Take 2 - Zeroseven Assessment

> **New Version**: https://zero7.netlify.com/

![screenshot](assets/screenshot2.gif)

> *Original Submitted Version*: https://zeroseven.netlify.com/ & https://bitbucket.org/rachel-wong/zeroseven/src

### :rotating_light: POST-FEEDBACK UPDATES

1. Resolved HTML5validator errors and most warnings.

2. Improved accessibility score in Lighthouse report

3. Tightened up some margin issues when the bootstrap `mb-md-5` was not working properly

4. *Still working on a **CSS-only** offcanvas menu. Current prototypes still rely on javascript*

5. ***Not Resolved***: IE11 spacing errors. I checked [lambdatest](https://www.lambdatest.com/) screenshots and experimented with user-agent-strings on safari browser. Still investigating ways of checking for browser compatibility on a mac without installing a VM.

### What problems this second version solved

* Bootstrap grid issues somewhat resolved by bringing in a consistent grid for services, footer, client-news across responsive screen sizes. This removes visual-guestimating and enabled greater accuracy.

* Fixed the issue where the carousel image does not fit fully vertically when orientation changes from portrait view to landscape. Now enforced a minimum banner height to images as well as the carousel container itself.

***Additional fixes***

* Fixed uneven top and bottom margin on the services row. See next section for caveat.

* Fixed uneven top and bottom margin on the footer row.

* For Services row, resolved the buggy extra column split so now the columns degrade evenly from 4 columns (desktop) to 2 columns (tab-land) to 1 column (mobile).  

* At footer, added space between the app download badges at full-size.

### What remains to be solved

* At smaller screensizes, I have yet to arrive at a nested grid layout that will allow a separate background colour for the client testimonial section over the news panels. Right now both section featuers in grey throughout.

* Reliable way to handle moving the footer and nav links to the scotch-panel.

* `object-fit` and `object-position` could not position the services panel images in the centre for tab-land, tab-port responsive sizes.

* Unable to use bootstrap `mb-md-5` to configure `margin-bottom` for services panels at tab-land tab-port sizes. Using children pseudo elements also did not work. Current workaround is to use id to target. Welcome feedback as to how to go about resolving this. 

* Unable to resolve the black svg for IE11. Looked at modernizr.

#### Responsiveness handler

0 - 600px:      Phone
600 - 900px:    Tablet landscape
900 - 1200px:   Tablet potrait
[1200 - 1800] is where our normal styles apply. no need for a media query
1800px +:       Large desktops

`$breakpoint` argument choices:
- phone
- tab-port
- tab-land
- big-desktop

ORDER: Base & typography > general layout + grid > page layout > components

1em = 16px