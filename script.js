$(function () {
  $('#scotch-panel').scotchPanel({
    containerSelector: 'body',
    direction: 'right',
    duration: 300,
    transition: 'ease',
    clickSelector: '.toggle-panel',
    distanceX: '80%',
    enableEscapeKey: true
  })
})

// Toggle Close & Burger - Imperfect because you need to click directly on the icon and not just the toggle area (could get stuck on either one)
function togglePanelIcon(x) {
  x.classList.toggle("fa-times")
}

// move footer, nav, searchbar when at TAB-PORT size
window.addEventListener("resize", function (event) {
  if (document.body.clientWidth < 900) {
    document.getElementById('navbar').appendChild(document.getElementById('registerTag'))
  } else {
    document.getElementById('registerSpace').appendChild(document.getElementById('registerTag'))
  }
})

// This should enable the div to move more reliably when window loads on different mobile devices/orientation changes
if (window.innerWidth < 900) {
  document.getElementById('navbar').appendChild(document.getElementById('registerTag'))
} else {
  document.getElementById('registerSpace').appendChild(document.getElementById('registerTag'))
}